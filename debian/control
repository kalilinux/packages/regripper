Source: regripper
Section: utils
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Devon Kearns <dookie@kali.org>,
           Ben Wilson <g0tmi1k@kali.org>,
           Sophie Brun <sophie@offensive-security.com>,
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Vcs-Git: https://gitlab.com/kalilinux/packages/regripper.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/regripper

Package: regripper
Architecture: i386 amd64
Depends: perl,
         wine,
         kali-defaults (>= 2019.3.6),
         ${shlibs:Depends},
         ${misc:Depends},
Description: Windows registry forensics tool
 RegRipper is an open source tool, written in Perl, for extracting/parsing
 information (keys, values, data) from the Registry and presenting it for
 analysis.
 .
 RegRipper consists of two basic tools, both of which provide similar
 capability. The RegRipper GUI allows the analyst to select a hive to parse, an
 output file for the results, and a profile (list of plugins) to run against the
 hive. When the analyst launches the tool against the hive, the results go to
 the file that the analyst designated. If the analyst chooses to parse the
 System hive, they might also choose to send the results to system.txt. The GUI
 tool
 will also create a log of it's activity in the same directory as the output
 file, using the same file name but using the .log extension (i.e., if the
 output is written to system.txt, the log will be written to system.log).
