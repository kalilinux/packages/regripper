#-----------------------------------------------------------
# specaccts.pl
# Gets contents of SpecialAccounts\UserList key
# 
# History
#   20120803 - updated to cover 32- and 64-bit systems
#   20100223 - created
#
# References
#   http://www.microsoft.com/security/portal/Threat/Encyclopedia/
#          Entry.aspx?Name=Trojan%3AWin32%2FStarter
#
#   http://www.microsoft.com/Security/portal/Threat/Encyclopedia/
#          Entry.aspx?Name=TrojanSpy%3AWin32%2FUrsnif.gen!H&ThreatID=-2147343835
#
#   MMPC write-up on EyeStye
#   http://www.microsoft.com/en-us/download/details.aspx?id=30399 
#
#  Some malware (and possibly attackers) use this key to maintain a 
#  peristent backdoor, by creating a user account and adding a value beneath 
#  this key; this causes the account to not be visible on the Welcome screen
#
# copyright 2012 Quantum Analytics Research, LLC
#-----------------------------------------------------------
package specaccts;
use strict;

my %config = (hive          => "Software",
              osmask        => 22,
              hasShortDescr => 1,
              hasDescr      => 0,
              hasRefs       => 0,
              version       => 20120803);

sub getConfig{return %config}

sub getShortDescr {
	return "Gets contents of SpecialAccounts\\UserList key";	
}
sub getDescr{}

sub getHive {return $config{hive};}
sub getVersion {return $config{version};}

my $VERSION = getVersion();

sub pluginmain {
	my $class = shift;
	my $hive = shift;
	::logMsg("Launching specaccts v.".$VERSION);
	::rptMsg("specaccts v.".$VERSION); # banner
    ::rptMsg("(".getHive().") ".getShortDescr()."\n"); # banner
	my $reg = Parse::Win32Registry->new($hive);
	my $root_key = $reg->get_root_key;

	my @key_paths = ("Microsoft\\Windows NT\\CurrentVersion\\Winlogon\\SpecialAccounts\\UserList",
									 "Wow6432Node\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon\\SpecialAccounts\\UserList");
	foreach my $key_path (@key_paths) {
		my $key;
		if ($key = $root_key->get_subkey($key_path)) {
			::rptMsg($key_path);
			::rptMsg("LastWrite Time ".gmtime($key->get_timestamp())." (UTC)");
			::rptMsg("");
			my %apps;
			my @vals = $key->get_list_of_values();
			if (scalar(@vals) > 0) {
				foreach my $v (@vals) {
					::rptMsg(sprintf "%-20s 0x%x",$v->get_name(),$v->get_data());
				}
			}
			else {
				::rptMsg($key_path." has no subkeys.");
			}
		}
		else {
			::rptMsg($key_path." not found.");
		}
	}
}
1;