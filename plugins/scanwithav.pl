#-----------------------------------------------------------
# scanwithav.pl
#
# Category: Malware
#
# History:
#  20130117 - created
#
# References:
#  http://support.microsoft.com/kb/883260
#
# copyright 2013 Quantum Analytics Research, LLC
# Author: H. Carvey, keydet89@yahoo.com
#-----------------------------------------------------------
package scanwithav;
use strict;

my %config = (hive          => "Software",
              hasShortDescr => 1,
              hasDescr      => 0,
              hasRefs       => 1,
              osmask        => 22,
              version       => 20130117);

sub getConfig{return %config}
sub getShortDescr {
	return "Checks ScanWithAV value in Software hive, per KB 883260";	
}
sub getDescr{}
sub getHive {return $config{hive};}
sub getVersion {return $config{version};}

my $VERSION = getVersion();

sub pluginmain {
	my $class = shift;
	my $hive = shift;
	::logMsg("Launching scanwithav v.".$VERSION);
	::rptMsg("Launching scanwithav v.".$VERSION);
	::rptMsg("(".$config{hive}.") ".getShortDescr()."\n"); # banner
	my $reg = Parse::Win32Registry->new($hive);
	my $root_key = $reg->get_root_key;

	my $key_path = 'Microsoft\\Windows\\CurrentVersion\\Policies\\Attachments';
  my $key;
  if ($key = $root_key->get_subkey($key_path)) {
	  ::rptMsg($key_path);
	  ::rptMsg("LastWrite Time ".gmtime($key->get_timestamp())." (UTC)");
	  ::rptMsg("");
	  
	  my $scan;
	  eval {
	  	$scan = $key->get_value("ScanWithAntiVirus")->get_data();
	  	::rptMsg("ScanWithAntiVirus = ".$scan);
	  	::rptMsg("");
	  	::rptMsg("Per MS KB 883260, 1 = Off, 2 = Optional, 3 = On");
	  };
	  if ($@) {
	  	::rptMsg("ScanWithAntiVirus value not found.");
	  }
	 
	}
	else {
 		::rptMsg($key_path." not found.");
	}
} 
1;